    var natural = require('natural');
    var classifier = new natural.BayesClassifier();
    //read file csv
    const csv = require('csv-parser');
    const fs = require('fs');
    // write csv
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;

    //header export file
    const csvWriter = createCsvWriter({
      path: 'out2.csv',
      header: [
        {id: 'id', title: 'id'},
        {id: 'comment_text', title: 'comment_text'},
        {id: 'toxic', title: 'toxic'},
        // {id: 'severe_toxic', title: 'severe_toxic'},
        // {id: 'obscene', title: 'obscene'},
        // {id: 'threat', title: 'threat'},
        // {id: 'insult', title: 'insult'},
        // {id: 'identity_hate', title: 'identity_hate'},
      ]
    });
    // read file
    fs.createReadStream('train.csv')
    .pipe(csv())
    .on('data', (row) => {
      let data = [];
      let data2 =[];
      data.push(row);
      data.map(item => {
        if(item.toxic==="1" || item.severe_toxic==="1" ||item.obscene==="1" ||  item.threat==="1" || item.insult==="1" || item.identity_hate==="1"){
            data2.push({
            id: item.id,
            comment_text: item.comment_text,
            toxic: 1
          });
        }
        if(item.toxic==="0" && item.severe_toxic==="0" && item.obscene==="0" &&  item.threat==="0" && item.insult==="0" && item.identity_hate==="0"){
            data2.push({
            id: item.id,
            comment_text: item.comment_text,
            toxic: 0
          });
        }
      });
      csvWriter
      .writeRecords(data2)
      .then(()=> console.log('The CSV file was written successfully'));
    })
    .on('end', () => {
      console.log('CSV file successfully processed');
    });